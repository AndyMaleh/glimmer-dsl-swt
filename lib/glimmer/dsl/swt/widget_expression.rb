require 'glimmer'
require 'glimmer/dsl/expression'
require 'glimmer/dsl/parent_expression'

module Glimmer
  module DSL
    module SWT
      class WidgetExpression < Expression
        include ParentExpression
  
        EXCLUDED_KEYWORDS = %w[shell display tab_item]
  
        def can_interpret?(parent, keyword, *args, &block)
          !EXCLUDED_KEYWORDS.include?(keyword) and
            parent.respond_to?(:swt_widget) and #TODO change to composite?(parent)
            Glimmer::SWT::WidgetProxy.widget_exists?(keyword)
        end
  
        def interpret(parent, keyword, *args, &block)
          Glimmer::SWT::WidgetProxy.create(keyword, parent, args)
        end
        
        def add_content(parent, &block)
          super
          parent.post_add_content
        end
        
      end
    end
  end
end

require 'glimmer/swt/widget_proxy'
require 'glimmer/swt/scrolled_composite_proxy'
require 'glimmer/swt/tree_proxy'
require 'glimmer/swt/table_proxy'
require 'glimmer/swt/table_column_proxy'
