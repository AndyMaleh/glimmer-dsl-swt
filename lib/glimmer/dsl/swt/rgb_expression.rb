require 'glimmer/dsl/swt/color_expression'
require 'glimmer/swt/color_proxy'
require 'glimmer/swt/display_proxy'

module Glimmer
  module DSL
    module SWT
      class RgbExpression < ColorExpression
      end
    end
  end
end
